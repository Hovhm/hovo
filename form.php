<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <form method="POST" action="http://localhost/Hovo/session/">
        <div class="form-group">
            <label for="name">Name</label>
            <input name="name"
                   type="text"
                   class="form-control <?=isset($_SESSION['old_name']) ? 'border border-danger' : ''?>"
            id="name"
                   placeholder="Enter name"
                   value="<?=isset($_SESSION['old_name']) ? $_SESSION['old_name'] : ''?>"
            >
            <?php
                if(isset($_SESSION['error_name']))
                    {
                        echo "<span class='text-danger'>{$_SESSION['error_name']}</span>";
                    }
            ?>
        </div>
        <div class="form-group">
            <label for="surname">Surname</label>
            <input name="surname" type="text" class="form-control" id="surname" placeholder="Enter name" value="">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Enter email">
            <?php
                if(isset($_SESSION['error_email']))
                {
                    echo "<span class='text-danger'>{$_SESSION['error_email']}</span>";
                }
            ?>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password">
            <?php
            if(isset($_SESSION['error_password']))
            {
                echo "<span class='text-danger'>{$_SESSION['error_password']}</span>";
            }
            ?>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>

<?php
session_destroy();
?>
