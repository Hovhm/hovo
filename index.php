<?php

session_start();
require_once ('function.php');
$users = require_once ('users.php');

if(emailExists($users,$_POST['email'])){
    $_SESSION['error_email'] = 'email already exists!';
    redirect('http://localhost/Hovo/session/form.php');
};
if(!checkPassword($_POST['password'])){
    $_SESSION['error_password'] = 'min length 5!';
    redirect('http://localhost/Hovo/session/form.php');
};
if(!checkName($_POST['name'])){
    $_SESSION['error_name'] = 'min length 5!';
    $_SESSION['old_name'] = $_POST['name'];
    redirect('http://localhost/Hovo/session/form.php');
}

/*
 * <?=444?>  <?php echo 444; ?>
 * */